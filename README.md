# HTML5 2 player pong #

This is a learning project to understand game development using HTML5 canvas and JavaScript.
Playable at http://tunzor.com/games/pong

## Learning points ##
* Update and draw functions to update game model and render to canvas 
* Keyboard input in JS
* Simple collision detection
* Drawing with HTML5 canvas